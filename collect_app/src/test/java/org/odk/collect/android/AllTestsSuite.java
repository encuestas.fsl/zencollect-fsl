package ec.cyberzen.collect.android;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ec.cyberzen.collect.android.activities.MainActivityTest;
import ec.cyberzen.collect.android.utilities.CompressionTest;
import ec.cyberzen.collect.android.utilities.PermissionsTest;
import ec.cyberzen.collect.android.utilities.TextUtilsTest;

/**
 * Suite for running all unit tests from one place
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        //Name of tests which are going to be run by suite
        MainActivityTest.class,
        PermissionsTest.class,
        TextUtilsTest.class,
        CompressionTest.class
})

public class AllTestsSuite {
    // the class remains empty,
    // used only as a holder for the above annotations
}
