package ec.cyberzen.collect.android.http;

import ec.cyberzen.collect.android.http.okhttp.OkHttpOpenRosaServerClientFactory;
import ec.cyberzen.collect.android.http.openrosa.OpenRosaServerClientFactory;
import ec.cyberzen.collect.android.utilities.Clock;

import okhttp3.OkHttpClient;
import okhttp3.tls.internal.TlsUtil;

public class OkHttpOpenRosaServerClientFactoryTest extends OpenRosaServerClientFactoryTest {

    @Override
    protected OpenRosaServerClientFactory buildSubject(Clock clock) {
        OkHttpClient.Builder baseClient = new OkHttpClient.Builder()
                .sslSocketFactory(
                        TlsUtil.localhost().sslSocketFactory(),
                        TlsUtil.localhost().trustManager());
        
        return new OkHttpOpenRosaServerClientFactory(baseClient, clock);
    }

    @Override
    protected Boolean useRealHttps() {
        return true;
    }
}
