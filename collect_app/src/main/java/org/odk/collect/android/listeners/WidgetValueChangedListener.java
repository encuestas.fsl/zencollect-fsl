package ec.cyberzen.collect.android.listeners;

import ec.cyberzen.collect.android.widgets.QuestionWidget;

public interface WidgetValueChangedListener {
    void widgetValueChanged(QuestionWidget changedWidget);
}
