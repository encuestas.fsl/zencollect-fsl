package ec.cyberzen.collect.android.utilities;

public interface DependencyProvider<T> {
    T provide();
}
