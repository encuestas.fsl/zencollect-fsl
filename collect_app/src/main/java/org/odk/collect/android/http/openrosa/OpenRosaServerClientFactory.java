package ec.cyberzen.collect.android.http.openrosa;

import androidx.annotation.Nullable;

import ec.cyberzen.collect.android.http.HttpCredentialsInterface;

public interface OpenRosaServerClientFactory {

    OpenRosaServerClient create(String schema, String userAgent, @Nullable HttpCredentialsInterface credentialsInterface);
}
