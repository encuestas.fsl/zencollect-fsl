package ec.cyberzen.collect.android.http;

public interface HttpCredentialsInterface {
    String getUsername();

    String getPassword();
}
