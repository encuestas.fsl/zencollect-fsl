package ec.cyberzen.collect.android.support;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import ec.cyberzen.collect.android.application.Collect;
import ec.cyberzen.collect.android.injection.config.AppDependencyComponent;
import ec.cyberzen.collect.android.logic.FormController;

public final class CollectHelpers {

    private CollectHelpers() {}

    public static FormController waitForFormController() throws InterruptedException {
        if (Collect.getInstance().getFormController() == null) {
            do {
                Thread.sleep(1);
            } while (Collect.getInstance().getFormController() == null);
        }

        return Collect.getInstance().getFormController();
    }

    public static AppDependencyComponent getAppDependencyComponent() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Collect application = (Collect) context;
        return application.getComponent();
    }
}
