# ZenCollect

![Plataforma](https://img.shields.io/badge/platform-Android-blue.svg)

[![Licencia](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)


Con ZenCollect puedes automatizar el trabajo de recolección de datos en campo, usalo sin conexión a internet y sincroniza con el servidor tus respuestas en cuanto sea posible.

Usa tipos de variables multimedia para tomar fotos, vídeos y gps

## Configurando su entorno de desarrollo

1. Descargue e instale [Git](https://git-scm.com/downloads) y agréguelo a su RUTA

1. Descargue e instale [Android Studio](https://developer.android.com/studio/index.html)

1. Bifurca el proyecto ([por qué y cómo bifurcar](https://help.github.com/articles/fork-a-repo/))

1. Clone el proyecto localmente. En la línea de comando:

        git clone https://gitlab.com/jfinlay/zencollect-fsl

    Si prefiere no usar la línea de comandos, puede usar Android Studio para crear un nuevo proyecto desde el control de versiones usando `https://github.com/jfinlay/zencollect-fsl`.

1. Abra el proyecto en la carpeta de su clon de Android Studio. Para ejecutar el proyecto, haga clic en la flecha verde en la parte superior de la pantalla.

1. Asegúrese de que puede ejecutar pruebas unitarias ejecutando todo en `collect_app/src/test/java` en Android Studio o en la línea de comandos:

    `` `
    ./gradlew testDebug
    `` `

1. Asegúrese de que puede ejecutar pruebas instrumentadas ejecutando todo en `collect_app/src/androidTest/java` en Android Studio o en la línea de comandos:

    `` `
    ./gradlew connectedAndroidTest
    `` `
    
